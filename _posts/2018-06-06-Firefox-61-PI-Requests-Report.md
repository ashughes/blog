---
layout: post
title: "Firefox 61 Product Integrity Requests Report"
date: 2018-06-06 14:50:00 -0700
---
## What is this?
Now that Firefox 61 has moved on to Beta, and continuing in the tradition started on 
[January 15, 2018](http://ashughes.com//jekyll/update/2018/01/16/Firefox-60-Product-Integrity-Requests-Report.html), 
this is the report on PI Requests for Firefox 61.

In general this report tracks three categories of requests for each milestone:
* "After Deadline" are requests filed after the deadline for the current 
  milestone but before the "PI request deadline" email goes out for the next 
  milestone.
* "Within Deadline" are requests filed in the week long window between the 
  "PI request deadline" email going out and the deadline for the current milestone.
* "Total" is quite simply an aggregate of the two.
There will be some inconsistency in this data with regards to the milestone as 
not every request filed within a milestone window is not necessarily tied to that 
exact milestone. However this is the closest approximation we can make with the 
data given at this time; a failing that I hope to address in the next iteration 
of the PI requests system.

## Firefox 58, 59, 60, 61
Key Dates for Firefox 61:
* March 7, 2018: Notice of Deadline to Submit PI Requests
* March 15, 2018: Deadline to Submit PI Requests
* May 7, 2018: Merge to Beta

![Product Integrity Requests Received](http://ashughes.com/assets/img/2018-06-06-Firefox-61-Product-Integrity-Received-Requests.png)

By and large we're getting most of our requests after the deadline 
(76% this cycle, 4% improvement over last cycle). It is possible that some of 
the "after deadline" number includes requests for the next milestone, 
particularly if requested toward the end of the current milestone. Unfortunately 
there is no concrete way for me to know this based on the information we 
currently record. Regardless I would like to see a trend over time of getting 
the majority of our requests prior to the deadline. Time will tell.

![Product Integrity Requests 48-Hour Performance](http://ashughes.com/assets/img/2018-06-06-Firefox-61-Product-Integrity-48h-SLA.png)

![Product Integrity Requests One-Week Performance](http://ashughes.com/assets/img/2018-06-06-Firefox-61-Product-Integrity-1w-SLA.png)

This cycle we missed our SLA of responding to 80% of requests within 48 hours 
(72% overall) and we missed our SLA of responding to 100% of requests 
within one week (96% overall). This cycle we tracked better with requests filed 
within the deadline than we did with requests following the deadline.

![Product Integrity Requests One-Week Performance](http://ashughes.com/assets/img/2018-06-06-Firefox-61-Product-Integrity-Average-Response-Time.png)

Now that we're 4 cycles in I can start to report on our average response time
to requests. After 3 cycles of trending up on requests within the deadline we
are now down to 42 hours to respond to these requests on average compared to
44 hours for requests filed after the deadline. For all requests since we started
to track this data, going back to Firefox 58, we are averaging 37 hours to
respond to incoming requests.

That's all for today. I'll be back in a couple months, toward the end of the Firefox 62 cycle.

Have questions or feedback about this report or the PI Requests system in general? [Please get in touch](mailto:ahughes@mozilla.com).
