---
layout: post
title: "Firefox 60 Product Integrity Requests Report"
date: 2018-01-15 23:28:00 -0700
categories: jekyll update
---
Late last year I was putting out weekly reports on the number of requests 
Mozilla's Product Integrity group was receiving and how well we were tracking 
toward our self-imposed service-level agreement (respond to 90% within 48 hours).

The initial system we set up was only ever intended to be minimally viable and 
has not scaled well, although that's probably to be expected. There's been quite
a lot of growing pains so I've been tasked with taking it to the next level.

### What does that have to do with the weekly reports?
Going forward I have decided to stop giving reports on a weekly basis and 
instead try giving milestone-based reports. Unfortunately the data in the
current system is not easily tracked against milestones and so the data
presented in these reports is not 100% accurate.

In general I am tracking two categories of requests for each milestone, based 
very loosely on the dates:
* "After Deadline" are requests filed after the deadline for the current 
  milestone but before the "PI request deadline" email goes out for the next 
  milestone.
* "Within Deadline" are requests filed in the week long window between the 
  "PI request deadline" email going out and the deadline for the current milestone.
* "Total" is quite simply an aggregate of the two.
There will be some inconsistency in this data with regards to the milestone as not every request filed within a milestone window is not necessarily tied to that exact milestone. However this is the closest approximation we can make with the data given at this time; a failing that I hope to address in the next iteration of the PI requests system.

### Firefox 58, 59, 60
The deadline to submit PI requests for Firefox 60 passed on January 7, 2018.

![Product Integrity Requests Received](http://ashughes.com/assets/img/2018-01-15-Firefox-60-Product-Integrity-Requests-Report-image001.png)

By and large we're getting most of our requests after the deadline. It is possible that some of the "after deadline" number includes requests for the next milestone, particularly if requested toward the end of the current milestone. Unfortunately there is no concrete way for me to know this based on the information we currently record. Regardless I would like to see a trend over time of getting the majority of our requests prior to the deadline. Time will tell.

![Product Integrity Requests Performance](http://ashughes.com/assets/img/2018-01-15-Firefox-60-Product-Integrity-Requests-Report-image002.png)

In terms of our SLA performance we're tracking better with requests made after the deadline than those filed within the week-long deadline window. Granted the lower volume of requests within the deadline window could be an impact. Currently we're tracking within 10% of our SLA which I'd like to see improved but is actually better than I thought we'd be doing at this point, considering the number of requests we receive and the number of resources we have available.

That's all for today. I'll be back toward the end of the Firefox 60 cycle to report on how that milestone shook out and to look forward to Firefox 61.

Have questions or feedback about this report or the PI Requests system in general? Please get in touch.