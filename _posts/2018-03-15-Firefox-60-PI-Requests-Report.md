---
layout: post
title: "Firefox 60 Product Integrity Requests Report"
date: 2018-03-15 14:50:00 -0700
---
## What is this?
Now that Firefox 60 has moved on to Beta, and continuing in the tradition started on 
[January 15, 2018](http://ashughes.com//jekyll/update/2018/01/16/Firefox-60-Product-Integrity-Requests-Report.html), 
this is the report on PI Requests for Firefox 60.

In general this report tracks three categories of requests for each milestone:
* "After Deadline" are requests filed after the deadline for the current 
  milestone but before the "PI request deadline" email goes out for the next 
  milestone.
* "Within Deadline" are requests filed in the week long window between the 
  "PI request deadline" email going out and the deadline for the current milestone.
* "Total" is quite simply an aggregate of the two.
There will be some inconsistency in this data with regards to the milestone as 
not every request filed within a milestone window is not necessarily tied to that 
exact milestone. However this is the closest approximation we can make with the 
data given at this time; a failing that I hope to address in the next iteration 
of the PI requests system.

## Firefox 58, 59, 60
Key Dates:
* January 7, 2018: Firefox 60 Notice of Deadline to Submit PI Requests
* January 10, 2018: Firefox 60 Deadline to Submit PI Requests
* March 12, 2019: Firefox 60 merges to Beta

![Product Integrity Requests Received](http://ashughes.com/assets/img/2018-03-15-Firefox-60-Product-Integrity-Requests-Report-requests.png)

By and large we're getting most of our requests after the deadline (**over 80% in this cycle**). It is possible that some of the "after deadline" number includes requests for the next milestone, particularly if requested toward the end of the current milestone. Unfortunately there is no concrete way for me to know this based on the information we currently record. Regardless I would like to see a trend over time of getting the majority of our requests prior to the deadline. Time will tell.

![Product Integrity Requests 48-Hour Performance](http://ashughes.com/assets/img/2018-03-15-Firefox-60-Product-Integrity-Report-48hour.png)

![Product Integrity Requests One-Week Performance](http://ashughes.com/assets/img/2018-03-15-Firefox-60-Product-Integrity-Report-oneweek.png)

This cycle we achieved our SLA of responding to 80% of requests within 48 hours 
(96% overall) and we also achieved our SAL of responding to 100% of requests 
within one week. This cycle we tracked better with requests filed within the
deadline than we did with requests following the deadline. 

That's all for today. I'll be back in a couple months, toward the end of the Firefox 61 cycle.

Have questions or feedback about this report or the PI Requests system in general? [Please get in touch](mailto:ahughes@mozilla.com).

