---
layout: post
title: "Mozilla Iris Update - November 2018"
date: 2018-12-03 10:00:00 -0800
---
## About this Report
This is a report on the progress made in the [Mozilla Iris](https://github.com/mozilla/iris/) project through __November 2018__.

[Mozilla Iris](https://github.com/mozilla/iris/) is a new test framework developed by Mozilla's Product Integrity group. Unlike other automated test frameworks, Iris aims to visually validate user interaction thereby freeing up manual testers from completing the mundane but very necessary tasks performed each and every product release.

___Iris running on MacOS___
![Mozilla Iris testing drag & drop to the address bar in Firefox](http://ashughes.com/assets/img/2018-12-03-Mozilla-Iris-Update-demo.gif)

You can find out more by visiting the [Mozilla Iris](https://github.com/mozilla/iris/) page on Github, dropping by the [#qa-automation](https://mozilla.slack.com/messages/C8U8VBWTU) channel on Slack or by emailing us at [iris@mozilla.com](mailto:iris@mozilla.com).

## Project Stats
![Overview of project status for the month of November 2018 (Source: github)](http://ashughes.com/assets/img/2018-12-03-Mozilla-Iris-Update-overview.png)

## Key Accomplishments
* Iris 1.0 released with a repository of 180 test cases and growing.
* Iris 2.0 planning and prototyping began including a port to Python 3 and a plethora of new features.
* We welcomed our Outreachy intern, Kristin Taylor, who will be redesigning the Iris control center.
* We began testing our first deployment with the Release QA team.
* 11 contributors landed
  * 188 new test cases
  * 89 enhancements to the core framework
  * 55 fixes to test bugs
  * 10 fixes to core bugs
* Found or confirmed [five](https://bugzilla.mozilla.org/buglist.cgi?short_desc={iris}&query_format=advanced&short_desc_type=allwordssubstr) real world Firefox bugs!

## What's Next
December is all about the Mozilla All Hands in Orlando, Florida. The team is busy getting ready to show off Iris to folks in attendance. Planning and prototyping for Iris 2.0 continues and Kristin will start on-boarding with the team.

